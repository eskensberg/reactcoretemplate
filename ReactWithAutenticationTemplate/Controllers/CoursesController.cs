﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace ReactWithAutenticationTemplate.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class CoursesController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<Course> Get()
        {
            var rng = new Random();

            string[] Courses = {
                "React-Redux", "C#", "Visual Studio", "JavaScript", "Html", "SCSS", "JQuery"
            };

            return Enumerable.Range(1, 5).Select(index => new Course
            {
                Id = index,
                Slug = Courses[rng.Next(Courses.Length)],
                Title = Courses[index],
                AuthorId = rng.Next(1, 4),
                Category = Courses[rng.Next(Courses.Length)],
            })
            .ToArray();


        }
    }

}