﻿using System;

namespace ReactWithAutenticationTemplate.Controllers
{
    public class Course
    {
        public int Id { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
        public int AuthorId { get; set; }
        public string Category { get; set; }
    }

   public class Author
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }


}