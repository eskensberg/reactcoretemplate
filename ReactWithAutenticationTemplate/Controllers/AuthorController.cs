﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace ReactWithAutenticationTemplate.Controllers
{
    //[Authorize]
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class AuthorsController : ControllerBase
    {
        private static readonly string[] Summaries = { "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching" };

        [HttpGet]
        public IEnumerable<Author> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 3).Select(index => new Author
                {
                    Name = Summaries[rng.Next(Summaries.Length)],
                    Id = index
                })
                .ToArray();
        }
    }
}