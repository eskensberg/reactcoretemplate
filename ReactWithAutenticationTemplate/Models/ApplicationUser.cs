﻿using Microsoft.AspNetCore.Identity;

namespace ReactWithAutenticationTemplate.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}
