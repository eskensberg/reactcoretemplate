import React from "react";
import { Route } from "react-router";
import { Layout } from "./components/common/Layout";
import HomePage from "./components/HomePage";
import { FetchData } from "./components/FetchData";
import CoursesPage from "./components/courses/CoursesPage";
import { Counter } from "./components/Counter";
import AboutPage from "./components/about/AboutPage";
import AuthorizeRoute from "./components/api-authorization/AuthorizeRoute";
import ApiAuthorizationRoutes from "./components/api-authorization/ApiAuthorizationRoutes";
import { ApplicationPaths } from "./components/api-authorization/ApiAuthorizationConstants";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import "./custom.css";

function App() {
  return (
    <Layout>
      <Route exact path="/" component={HomePage} />
      <Route path="/courses" component={CoursesPage} />
      <Route path="/counter" component={Counter} />
      <Route path="/about" component={AboutPage} />
      <AuthorizeRoute path="/fetch-data" component={FetchData} />
      <Route
        path={ApplicationPaths.ApiAuthorizationPrefix}
        component={ApiAuthorizationRoutes}
      />
      <ToastContainer autoClose={3000} />
    </Layout>
  );
}
export default App;
